# README #

### What is this repository for? ###

A project that uses iOS 8's Photos library to fetch and retrieve Photos, allow user selection of said photos, and compresses these photos using the WebP format in memory. When the user want to view the selected photo, the photo is read from the WebP file format and displayed. 

### Note ###

This is setup to be iOS 8.3 and up compatible.