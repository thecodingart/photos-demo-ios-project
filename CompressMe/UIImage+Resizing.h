//
//  UIImage+Resizing.h
//  CompressMe
//
//  Created by Brandon Levasseur on 4/26/15.
//  Copyright (c) 2015 Brandon Levasseur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Resizing)

+ (UIImage *)cropImage:(UIImage*)image toSize:(CGSize)size;

@end
