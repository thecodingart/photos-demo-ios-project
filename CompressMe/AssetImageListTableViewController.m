//
//  AssetImageListTableViewController.m
//  CompressMe
//
//  Created by Brandon Levasseur on 4/26/15.
//  Copyright (c) 2015 Brandon Levasseur. All rights reserved.
//

#import "AssetImageListTableViewController.h"
#import "AssetImageTableViewCell.h"
#import "Asset.h"
#import "CoreDataStack.h"
#import "AssetImageDetailsViewController.h"

@interface AssetImageListTableViewController () <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchResultsController;

@end

@implementation AssetImageListTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSError *fetchingError;
    if (![self.fetchResultsController performFetch:&fetchingError]) {
        NSLog(@"There was an error fetching the data: %@", fetchingError);
        abort();
    }
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id<NSFetchedResultsSectionInfo> sectionInfo = self.fetchResultsController.sections[section];
    NSInteger numberOfRows = sectionInfo.numberOfObjects;
    return numberOfRows;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AssetImageTableViewCell *cell = (AssetImageTableViewCell *)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([AssetImageTableViewCell class]) forIndexPath:indexPath];
    [self configureAssetImageCell:cell forIndexPath:indexPath];
    
    return cell;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isMemberOfClass:[AssetImageDetailsViewController class]]) {
        AssetImageDetailsViewController *assetImageDetailsViewController = segue.destinationViewController;
        
        NSIndexPath *selectedRowIndexPath = [self.tableView indexPathForSelectedRow];
        Asset *asset = [self.fetchResultsController objectAtIndexPath:selectedRowIndexPath];
        
        assetImageDetailsViewController.asset = asset;
    }
}


- (void)configureAssetImageCell:(AssetImageTableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath
{
    Asset *asset = [self.fetchResultsController objectAtIndexPath:indexPath];
    cell.fileNameLabel.text = asset.fileName;
}

- (NSFetchedResultsController *)fetchResultsController
{
    if (!_fetchResultsController) {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([Asset class])];
        fetchRequest.fetchBatchSize = 30;
        
        NSSortDescriptor *importDateSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"importDate" ascending:YES];
        NSSortDescriptor *fileNameSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"fileName" ascending:YES];
        
        fetchRequest.sortDescriptors = @[importDateSortDescriptor, fileNameSortDescriptor];
        
        NSFetchedResultsController *fetchResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.coreDataStack.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
        
        fetchResultsController.delegate = self;
        _fetchResultsController = fetchResultsController;
        
    }
    
    return _fetchResultsController;
}


#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    switch (type) {
        case NSFetchedResultsChangeInsert:
        {
            [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
            break;
        case NSFetchedResultsChangeDelete:
        {
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
            break;
        case NSFetchedResultsChangeMove:
        {
            if (!newIndexPath) {
                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
                return;
            }
            
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
            break;
        case NSFetchedResultsChangeUpdate:
        {
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
        default:
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch (type) {
        case NSFetchedResultsChangeInsert:
        {
            
        }
            break;
        case NSFetchedResultsChangeDelete:
        {
            
        }
        default:
            break;
    }
}

@end
