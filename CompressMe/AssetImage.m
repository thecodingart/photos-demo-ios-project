//
//  AssetImage.m
//  CompressMe
//
//  Created by Brandon Levasseur on 4/27/15.
//  Copyright (c) 2015 Brandon Levasseur. All rights reserved.
//

#import "AssetImage.h"
#import "Asset.h"


@implementation AssetImage

@dynamic image;
@dynamic asset;

@end
