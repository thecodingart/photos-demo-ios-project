//
//  Asset.m
//  CompressMe
//
//  Created by Brandon Levasseur on 4/27/15.
//  Copyright (c) 2015 Brandon Levasseur. All rights reserved.
//

#import "Asset.h"
#import "AssetImage.h"


@implementation Asset

@dynamic fileName;
@dynamic importDate;
@dynamic assetImage;

@end
