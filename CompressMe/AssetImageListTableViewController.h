//
//  AssetImageListTableViewController.h
//  CompressMe
//
//  Created by Brandon Levasseur on 4/26/15.
//  Copyright (c) 2015 Brandon Levasseur. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CoreDataStack;

@interface AssetImageListTableViewController : UITableViewController

@property (strong, nonatomic) CoreDataStack *coreDataStack;

@end
