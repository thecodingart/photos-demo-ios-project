//
//  AssetCollectionsTableViewCell.h
//  CompressMe
//
//  Created by Brandon Levasseur on 4/25/15.
//  Copyright (c) 2015 Brandon Levasseur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssetCollectionsTableViewCell : UITableViewCell

@end
