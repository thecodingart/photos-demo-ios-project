//
//  AssetCollectionViewCell.m
//  CompressMe
//
//  Created by Brandon Levasseur on 4/25/15.
//  Copyright (c) 2015 Brandon Levasseur. All rights reserved.
//

#import "AssetCollectionViewCell.h"
#import "CheckmarkSelectionView.h"

@interface AssetCollectionViewCell ()
@property (weak, nonatomic) IBOutlet CheckmarkSelectionView *checkmarkSelectionView;

@end

@implementation AssetCollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    _checkmarkSelectionView.selected = NO;
}

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    self.imageView.highlighted = selected;
    self.checkmarkSelectionView.selected = selected;
}

@end
