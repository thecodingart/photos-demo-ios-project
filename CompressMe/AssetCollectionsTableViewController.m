//
//  AssetCollectionsTableViewController.m
//  CompressMe
//
//  Created by Brandon Levasseur on 4/25/15.
//  Copyright (c) 2015 Brandon Levasseur. All rights reserved.
//

#import "AssetCollectionsTableViewController.h"
#import "AssetCollectionsTableViewCell.h"
#import "AssetsListCollectionViewController.h"
#import "AssetImageListTableViewController.h"
@import Photos;

#warning Please note this does not handle/fetch asset collections at this time

@interface AssetCollectionsTableViewController ()

typedef NS_ENUM(NSInteger, AssetGroupSectionType) {
    AssetGroupSectionTypeSaved = 0,
    AssetGroupSectionTypeAll,
    AssetGroupSectionTypeAlbums
};

@property (strong, nonatomic) NSArray *sectionNames;
@property (strong, nonatomic) PHFetchResult *albums;

@end

@implementation AssetCollectionsTableViewController



- (void)awakeFromNib
{
    [super awakeFromNib];
    [self customInit];
}

- (void)customInit
{
    _sectionNames =  @[@"", @"All Photos", @"Albums"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (!self.selectedAssets) {
        self.selectedAssets = [NSMutableArray new];
    }
    
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            switch (status) {
                case PHAuthorizationStatusAuthorized:
                {
                    [self reloadData];
                }
                    break;
                    
                default:
                {
                    [self showNoAccessAlertAndCancel];
                }
                    break;
            }
        });
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sectionNames.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 0;
    
    switch (section) {
        case AssetGroupSectionTypeSaved:
        case AssetGroupSectionTypeAll:
        {
            numberOfRows = 1;
        }
            break;
        case AssetGroupSectionTypeAlbums:
        {
            numberOfRows = self.albums.count;
        }
            break;
            
    }
    return numberOfRows;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AssetCollectionsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([AssetCollectionsTableViewCell class]) forIndexPath:indexPath];
    [self configureAssetCollectionCell:cell forIndexPath:indexPath];
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return self.sectionNames[section];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    switch (indexPath.section) {
        case AssetGroupSectionTypeSaved:
        {
            AssetImageListTableViewController *assetImageListTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([AssetImageListTableViewController class])];
            assetImageListTableViewController.coreDataStack = self.coreDataStack;
            [self showViewController:assetImageListTableViewController sender:self];
        }
            break;
        case AssetGroupSectionTypeAll:
        {
            AssetsListCollectionViewController *assetsListCollectionViewController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([AssetsListCollectionViewController class])];
            PHFetchOptions *options = [PHFetchOptions new];
            NSSortDescriptor *creationDateSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES];
            options.sortDescriptors = @[creationDateSortDescriptor];
            
            assetsListCollectionViewController.fetchResult = [PHAsset fetchAssetsWithOptions:options];
            assetsListCollectionViewController.title = @"All Photos";
            
            assetsListCollectionViewController.coreDataStack = self.coreDataStack;
            [self showViewController:assetsListCollectionViewController sender:self];
        }
            break;
        case AssetGroupSectionTypeAlbums:
        {
            AssetsListCollectionViewController *assetsListCollectionViewController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([AssetsListCollectionViewController class])];
            PHFetchOptions *options = [PHFetchOptions new];
            NSSortDescriptor *creationDateSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES];
            options.sortDescriptors = @[creationDateSortDescriptor];
            
            PHAssetCollection *assetCollection = self.albums[indexPath.row];
            assetsListCollectionViewController.fetchResult = [PHAsset fetchAssetsInAssetCollection:assetCollection options:options];
            assetsListCollectionViewController.title = assetCollection.localizedTitle;
            
            assetsListCollectionViewController.coreDataStack = self.coreDataStack;
            [self showViewController:assetsListCollectionViewController sender:self];
        }
            break;
        default:
            break;
    }
}

- (void)configureAssetCollectionCell:(AssetCollectionsTableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath
{
    NSString *sectionName = @"";
    NSString *sectionDetails = @"";
    
    switch (indexPath.section) {
        case AssetGroupSectionTypeSaved:
        {
            sectionName = @"Imported Photos";
            sectionDetails = [NSString stringWithFormat:@"%lu", (unsigned long)self.selectedAssets.count];
        }
            break;
        case AssetGroupSectionTypeAll:
        {
            sectionName = @"All Photos";
        }
            break;
        case AssetGroupSectionTypeAlbums:
        {
            PHAssetCollection *album = self.albums[indexPath.row];
            sectionName = album.localizedTitle;
            if (album.estimatedAssetCount != NSNotFound) {
                sectionDetails = [NSString stringWithFormat:@"%lu", (unsigned long)album.estimatedAssetCount];
            }
        }
            break;
    }
    
    cell.textLabel.text = sectionName;
    cell.detailTextLabel.text = sectionDetails;
}

//MARK: Reloading Data

- (void)reloadData
{
    self.albums = [PHCollectionList fetchTopLevelUserCollectionsWithOptions:nil];
    [self.tableView reloadData];
}

//MARK: Alerts

- (void)showNoAccessAlertAndCancel
{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"No Photo Permissions" message:@"Please grant photo permissions in Settings"  preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }]];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

@end
