//
//  WebPImageTransformer.m
//  CompressMe
//
//  Created by Brandon Levasseur on 4/25/15.
//  Copyright (c) 2015 Brandon Levasseur. All rights reserved.
//

#import "WebPImageTransformer.h"
#import "UIImage+WebP.h"
@import UIKit;


@implementation WebPImageTransformer

+ (Class)transformedValueClass
{
    return [NSData class];
}

+ (BOOL)allowsReverseTransformation
{
    return YES;
}

- (id)reverseTransformedValue:(id)value
{
    if (!value) {
        return nil;
    }
    
    UIImage *image = [UIImage imageWithWebPData:value];
    return image;
}

- (id)transformedValue:(id)value
{
    if (!value || ![value isKindOfClass:[UIImage class]]) {
        NSLog(@"The value is not a valid image");
        return nil;
    }
    
    UIImage *sourceImage = value;
    NSData *compressedImage = [UIImage convertToWebP:sourceImage quality:75 alpha:1.0 preset:WEBP_PRESET_PICTURE configBlock:nil error:nil];//[UIImage imageToWebP:sourceImage quality:75];//75 is default by Google
    
    return compressedImage;
}

@end
