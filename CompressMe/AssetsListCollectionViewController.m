//
//  AssetsListCollectionViewController.m
//  CompressMe
//
//  Created by Brandon Levasseur on 4/25/15.
//  Copyright (c) 2015 Brandon Levasseur. All rights reserved.
//

#import "AssetsListCollectionViewController.h"
#import "AssetCollectionViewCell.h"
#import "CoreDataStack.h"
#import "AssetImage.h"
#import "Asset.h"
#import "UIImage+Resizing.h"
@import Photos;

@interface AssetsListCollectionViewController ()

@property (strong, nonatomic) PHCachingImageManager *cachingImageManager;
@property (strong, nonatomic) NSMutableArray *cachedIndexes;
@property (assign, nonatomic) CGFloat previousFrameCenter;

@property (strong, nonatomic) dispatch_queue_t cacheOperationsQueue;
@property (assign, nonatomic) CGSize imageTargetSize;

@property (assign, nonatomic) CGSize importImageTargetSize;

@property (strong, nonatomic) NSMutableArray *selectedAssets;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *importBarButtonItem;

@end

@implementation AssetsListCollectionViewController


- (void)awakeFromNib
{
    [super awakeFromNib];
    [self customInit];
}

- (void)customInit
{
    _cachingImageManager = [PHCachingImageManager new];
    _cacheOperationsQueue = dispatch_queue_create("cacheOperationsQueue", DISPATCH_QUEUE_SERIAL);
    _cachedIndexes = [NSMutableArray new];
    _selectedAssets = [NSMutableArray new];
    _importImageTargetSize = CGSizeMake(128.0, 128.0);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.importBarButtonItem.enabled = self.selectedAssets.count > 0;
    self.collectionView.allowsMultipleSelection = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    CGFloat scale = [UIScreen mainScreen].scale;
    CGSize cellSize = ((UICollectionViewFlowLayout *)self.collectionViewLayout).itemSize;
    self.imageTargetSize = CGSizeMake(cellSize.width * scale, cellSize.height * scale);
    [self resetCache];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.fetchResult.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    AssetCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([AssetCollectionViewCell class]) forIndexPath:indexPath];
    
    [self configureAssetCell:cell forIndexPath:indexPath];
    
    return cell;
}

- (void)configureAssetCell:(AssetCollectionViewCell *)cell forIndexPath:(NSIndexPath *)indexPath
{
    PHAsset *asset = self.fetchResult[indexPath.item];
    PHImageRequestOptions *options = [PHImageRequestOptions new];
    options.networkAccessAllowed = YES;
    cell.imageView.image = nil;
    
    __weak AssetsListCollectionViewController *weakSelf = self;
    [self.cachingImageManager requestImageForAsset:asset targetSize:self.imageTargetSize contentMode:PHImageContentModeAspectFit options:options resultHandler:^(UIImage *result, NSDictionary *info) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (![[weakSelf.collectionView indexPathsForVisibleItems] containsObject:indexPath]) {
                return;
            }
            AssetCollectionViewCell *assetCell = (AssetCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
            assetCell.imageView.image = result;
        });

    }];
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PHAsset *asset = self.fetchResult[indexPath.item];
    [self.selectedAssets addObject:asset];
    
    if (self.selectedAssets.count > 0 && !self.importBarButtonItem.enabled) {
        self.importBarButtonItem.enabled = YES;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PHAsset *asset = self.fetchResult[indexPath.item];
    [self.selectedAssets removeObject:asset];
    
    if (self.selectedAssets.count == 0 && self.importBarButtonItem.enabled) {
        self.importBarButtonItem.enabled = NO;
    }
}


#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    dispatch_async(self.cacheOperationsQueue, ^{
        [self updateCache];
    });
}

#pragma mark - Caching

- (void)resetCache
{
    [self.cachingImageManager stopCachingImagesForAllAssets];
    [self.cachedIndexes removeAllObjects];
    self.previousFrameCenter = 0;
}

- (void)updateCache
{
    CGFloat currentFrameCenter = CGRectGetMidY(self.collectionView.bounds);
    if (fabs(currentFrameCenter - self.previousFrameCenter) < CGRectGetHeight(self.collectionView.bounds)/3) {
        // Haven't scrolled far enough yet
        return;
    }
    self.previousFrameCenter = currentFrameCenter;
    
    static NSInteger numOffscreenAssetsToCache = 60;
    
    NSArray * visibleIndexes = [self.collectionView.indexPathsForVisibleItems sortedArrayUsingSelector:@selector(compare:)];
    
    if (!visibleIndexes.count) {
        [self.cachingImageManager stopCachingImagesForAllAssets];
        return;
    }
    
    NSInteger firstItemToCache = ((NSIndexPath *)visibleIndexes[0]).item - numOffscreenAssetsToCache/2;
    firstItemToCache = MAX(firstItemToCache, 0);
    
    NSInteger lastItemToCache = ((NSIndexPath *)[visibleIndexes lastObject]).item + numOffscreenAssetsToCache/2;
    if (self.fetchResult) {
        lastItemToCache = MIN(lastItemToCache, self.fetchResult.count - 1);
    }
    else {
        lastItemToCache = MIN(lastItemToCache, self.selectedAssets.count - 1);
    }
    
    NSMutableArray * indexesToStopCaching = [[NSMutableArray alloc] init];
    NSMutableArray * indexesToStartCaching = [[NSMutableArray alloc] init];
    
    // Stop caching items we scrolled past
    for (NSIndexPath * index in self.cachedIndexes) {
        if (index.item < firstItemToCache || index.item > lastItemToCache) {
            [indexesToStopCaching addObject:index];
        }
    }
    [self.cachedIndexes removeObjectsInArray:indexesToStopCaching];
    
    NSArray *assetsToStopCaching = [self assetsAtIndexPaths:indexesToStopCaching];
    [self.cachingImageManager stopCachingImagesForAssets:assetsToStopCaching targetSize:self.imageTargetSize contentMode:PHImageContentModeAspectFill options:nil];
    
    // Start caching new items in range
    for (NSInteger i = firstItemToCache; i < lastItemToCache; i++) {
        NSIndexPath * index = [NSIndexPath indexPathForItem:i inSection:0];
        if (![self.cachedIndexes containsObject:index]) {
            [indexesToStartCaching addObject:index];
            [self.cachedIndexes addObject:index];
        }
    }
    
    NSArray *assetsToCache = [self assetsAtIndexPaths:indexesToStartCaching];
    [self.cachingImageManager startCachingImagesForAssets:assetsToCache targetSize:self.imageTargetSize contentMode:PHImageContentModeAspectFill options:nil];
    
}

- (NSArray *)assetsAtIndexPaths:(NSArray *)indexPaths
{
    
    NSMutableArray *assets = indexPaths.count > 0 ? [NSMutableArray arrayWithCapacity:indexPaths.count] : nil;
    
    for (NSIndexPath *indexPath in indexPaths) {
        PHAsset *asset = self.fetchResult[indexPath.item];
        [assets addObject:asset];
    }
    
    return assets;
}

- (IBAction)importImagesButtonPressed:(UIBarButtonItem *)sender
{
    self.view.window.userInteractionEnabled = NO;
    self.view.window.tintAdjustmentMode = UIViewTintAdjustmentModeDimmed;
    self.view.tintAdjustmentMode = UIViewTintAdjustmentModeNormal;
    
    UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:activityIndicatorView];
    [activityIndicatorView startAnimating];
    
    
    __weak AssetsListCollectionViewController *weakSelf = self;
    [self importSelectedAssets:self.selectedAssets withCompletion:^(BOOL success, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.navigationItem.rightBarButtonItem = nil; //We could reset the import button here, but there's no need since we're poping out
            weakSelf.view.tintAdjustmentMode = UIViewTintAdjustmentModeDimmed;
            weakSelf.view.window.userInteractionEnabled = YES;
            weakSelf.view.tintAdjustmentMode = UIViewTintAdjustmentModeAutomatic;
            weakSelf.view.window.tintAdjustmentMode = UIViewTintAdjustmentModeAutomatic;
            [weakSelf.navigationController popViewControllerAnimated:YES];
        });
        
    }];
}

- (void)importSelectedAssets:(NSArray *)assets withCompletion:(void(^)(BOOL success, NSError *error))completion
{
    NSManagedObjectContext *privateContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    privateContext.parentContext = self.coreDataStack.managedObjectContext;
    __weak AssetsListCollectionViewController *weakSelf = self;
    
    [privateContext performBlock:^{
        __block NSDate *importDate = [NSDate date];
        
        NSEntityDescription *assetEntity = [NSEntityDescription entityForName:NSStringFromClass([Asset class]) inManagedObjectContext:privateContext];
        NSEntityDescription *assetImageEntity = [NSEntityDescription entityForName:NSStringFromClass([AssetImage class]) inManagedObjectContext:privateContext];
        
        dispatch_group_t downloadGroup = dispatch_group_create();
        for (PHAsset *asset in assets){
            dispatch_group_enter(downloadGroup);
            PHImageRequestOptions *options = [PHImageRequestOptions new];
            options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
            options.networkAccessAllowed = true;
            
            [[PHImageManager defaultManager] requestImageDataForAsset:asset options:options resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info) {
                [privateContext performBlock:^{
                    if ([info[PHImageResultIsDegradedKey] boolValue]) {
                        return;
                    }
                    //PHImageFileURLKey is an undocumented key... so this may not exist later, but it's one of the hidden ways to get the file name we need
                    NSURL *fileURL = info[@"PHImageFileURLKey"];
                    NSArray *fileComponenets = [fileURL pathComponents];
                    NSString *fileName = fileComponenets.lastObject;
                    
                    UIImage *resultImage = [UIImage imageWithData:imageData];
                    
                    CGSize targetSize = weakSelf.importImageTargetSize;
                    
                    if (!CGSizeEqualToSize(resultImage.size, targetSize)) {
                        resultImage = [UIImage cropImage:resultImage toSize:targetSize];
                    }
                    
                    Asset *asset = [[Asset alloc] initWithEntity:assetEntity insertIntoManagedObjectContext:privateContext];
                    
                    asset.importDate = importDate;
                    asset.fileName = fileName;
                    
                    AssetImage *assetImage = [[AssetImage alloc] initWithEntity:assetImageEntity insertIntoManagedObjectContext:privateContext];
                    assetImage.image = resultImage;
                    
                    asset.assetImage = assetImage;
                    
                    dispatch_group_leave(downloadGroup);
                }];
            }];

        }
        
        
        dispatch_group_notify(downloadGroup, dispatch_get_main_queue(), ^{
            [privateContext performBlock:^{
                NSError *saveError;
                
                if (privateContext.hasChanges && ![privateContext save:&saveError]) {
                    
                    NSLog(@"Managed object context could not save %@, %@", saveError, saveError.userInfo);
                    if (completion) {
                        completion(NO, saveError);
                    }
                    return;
                }
                
                [weakSelf.coreDataStack saveContext];
                if (completion) {
                    completion(YES, saveError);
                }
            }];
        });
       

    }];
}


@end
