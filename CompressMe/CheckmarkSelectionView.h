//
//  CheckmarkSelectionView.h
//  CompressMe
//
//  Created by Brandon Levasseur on 4/25/15.
//  Copyright (c) 2015 Brandon Levasseur. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE
@interface CheckmarkSelectionView : UIView

@property (nonatomic, strong) IBInspectable UIColor *fillColor;
@property (nonatomic, strong) IBInspectable UIColor *checkmarkColor;
@property (nonatomic, strong) IBInspectable UIColor *ringColor;

@property (assign, nonatomic) IBInspectable BOOL selected;

@end
