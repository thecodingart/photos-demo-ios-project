//
//  AssetImageDetailsViewController.m
//  CompressMe
//
//  Created by Brandon Levasseur on 4/26/15.
//  Copyright (c) 2015 Brandon Levasseur. All rights reserved.
//

#import "AssetImageDetailsViewController.h"
#import "AssetImage.h"
#import "Asset.h"

@interface AssetImageDetailsViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation AssetImageDetailsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = self.asset.fileName;
    self.imageView.image = self.asset.assetImage.image;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
