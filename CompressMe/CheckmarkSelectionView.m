//
//  CheckmarkSelectionView.m
//  CompressMe
//
//  Created by Brandon Levasseur on 4/25/15.
//  Copyright (c) 2015 Brandon Levasseur. All rights reserved.
//

#import "CheckmarkSelectionView.h"

@implementation CheckmarkSelectionView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self customInit];
    }
    
    return self;
}

- (instancetype)init
{
    if (self = [super init]) {
        [self customInit];
    }
    return self;
}

- (void)customInit
{
    
}


//MARK: Getters/Setters
- (UIColor *)fillColor
{
    if (!_fillColor) {
        _fillColor = [UIColor colorWithRed: 0.078 green: 0.435 blue: 0.875 alpha: 1];
    }
    return _fillColor;
}

- (UIColor *)checkmarkColor
{
    if (!_checkmarkColor) {
        _checkmarkColor = [UIColor whiteColor];
    }
    return _checkmarkColor;
}

- (UIColor *)ringColor
{
    if (!_ringColor) {
        _ringColor = [UIColor whiteColor];
    }
    return _ringColor;
}

//MARK: Drawing methods

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    [self drawRect:rect selected:self.selected];
}

- (void)drawRect:(CGRect)rect selected:(BOOL)isSelected
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (!isSelected) {
        CGContextClearRect(context, rect);
        return;
    }
    
    
    UIColor* shadow2 = [UIColor blackColor];
    CGSize shadow2Offset = CGSizeMake(0.1, -0.1);
    CGFloat shadow2BlurRadius = 2.5;
    
    
    CGRect frame = self.bounds;
    
    
    CGRect group = CGRectMake(CGRectGetMinX(frame) + 3, CGRectGetMinY(frame) + 3, CGRectGetWidth(frame) - 6, CGRectGetHeight(frame) - 6);
    
    
    UIBezierPath* checkedOvalPath = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(CGRectGetMinX(group) + floor(CGRectGetWidth(group) * 0.00000 + 0.5), CGRectGetMinY(group) + floor(CGRectGetHeight(group) * 0.00000 + 0.5), floor(CGRectGetWidth(group) * 1.00000 + 0.5) - floor(CGRectGetWidth(group) * 0.00000 + 0.5), floor(CGRectGetHeight(group) * 1.00000 + 0.5) - floor(CGRectGetHeight(group) * 0.00000 + 0.5))];
    CGContextSaveGState(context);
    CGContextSetShadowWithColor(context, shadow2Offset, shadow2BlurRadius, shadow2.CGColor);
    [self.fillColor setFill];
    [checkedOvalPath fill];
    CGContextRestoreGState(context);
    
    [self.ringColor setStroke];
    checkedOvalPath.lineWidth = 1;
    [checkedOvalPath stroke];
    
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint: CGPointMake(CGRectGetMinX(group) + 0.27083 * CGRectGetWidth(group), CGRectGetMinY(group) + 0.54167 * CGRectGetHeight(group))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(group) + 0.41667 * CGRectGetWidth(group), CGRectGetMinY(group) + 0.68750 * CGRectGetHeight(group))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(group) + 0.75000 * CGRectGetWidth(group), CGRectGetMinY(group) + 0.35417 * CGRectGetHeight(group))];
    bezierPath.lineCapStyle = kCGLineCapSquare;
    
    [self.checkmarkColor setStroke];
    bezierPath.lineWidth = 1.3;
    [bezierPath stroke];
}

- (void)setSelected:(BOOL)selected
{
    _selected = selected;
    [self setNeedsDisplay];
}

@end
