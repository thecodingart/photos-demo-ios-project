//
//  AssetImageTableViewCell.m
//  CompressMe
//
//  Created by Brandon Levasseur on 4/26/15.
//  Copyright (c) 2015 Brandon Levasseur. All rights reserved.
//

#import "AssetImageTableViewCell.h"

@implementation AssetImageTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
