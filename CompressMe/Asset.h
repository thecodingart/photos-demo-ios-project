//
//  Asset.h
//  CompressMe
//
//  Created by Brandon Levasseur on 4/27/15.
//  Copyright (c) 2015 Brandon Levasseur. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class AssetImage;

@interface Asset : NSManagedObject

@property (nonatomic, retain) NSString * fileName;
@property (nonatomic, retain) NSDate * importDate;
@property (nonatomic, retain) AssetImage *assetImage;

@end
