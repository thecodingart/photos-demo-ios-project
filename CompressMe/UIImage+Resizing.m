//
//  UIImage+Resizing.m
//  CompressMe
//
//  Created by Brandon Levasseur on 4/26/15.
//  Copyright (c) 2015 Brandon Levasseur. All rights reserved.
//

#import "UIImage+Resizing.h"

@implementation UIImage (Resizing)

+ (UIImage *)cropImage:(UIImage*)image toSize:(CGSize)size
{
    CGFloat ratio = MIN(image.size.width / size.width, image.size.height / size.height);
    
    CGSize newSize = {image.size.width / ratio, image.size.height / ratio};
    CGPoint offset = {0.5 * (size.width - newSize.width), 0.5 * (size.height - newSize.height)};
    CGRect rect = {offset, newSize};
    
    UIGraphicsBeginImageContext(size);
    [image drawInRect:rect];
    UIImage * output = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return output;
}

@end
