//
//  AssetImage.h
//  CompressMe
//
//  Created by Brandon Levasseur on 4/27/15.
//  Copyright (c) 2015 Brandon Levasseur. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Asset;

@interface AssetImage : NSManagedObject

@property (nonatomic, retain) id image;
@property (nonatomic, retain) Asset *asset;

@end
